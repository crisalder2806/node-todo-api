const mongoose  = require('mongoose');
const validator = require('validator');
const jwt       = require('jsonwebtoken');
const _         = require('lodash');
const bcrypt    = require('bcryptjs');

const tokenKey = process.env.JWT_SECRET;
var UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,        
        trim: true,
        unique: true,
        minlength: 1,
        validate: {
            validator: validator.isEmail,
            message: 'Invalid email'
        }
        // match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address']
    },
    password: {
        type: String,
        required: true,
        minlength: 6
    },
    tokens: [{
        access: {
            type: String,
            required: true
        },
        token: {
            type: String,
            required: true
        }
    }]
})

UserSchema.methods.toJSON = function () {
    var user = this;
    var userObject = user.toObject();

    return _.pick(userObject, ['_id', 'email']);
}

UserSchema.methods.generateAuthToken = function () {
    var user = this;
    var access = 'auth';
    var token = jwt.sign({_id: user._id.toHexString(), access}, tokenKey).toString();

    user.tokens.push({access, token});

    return user.save().then(() => {
        return token;
    })
}

UserSchema.pre('save', function (next) {
    var user = this;

    if (user.isModified('password')) {
        var salt = bcrypt.genSaltSync(10);        
        var hash = bcrypt.hashSync(user.password, salt);
        user.password = hash;
        next();
    } else {
        next();
    }
})

UserSchema.statics.findByToken = function (token) {
    var User = this;
    var decoded;

    try {
        decoded = jwt.verify(token, tokenKey);
    } catch (e) {
        return Promise.reject('test')
    }

    return User.findOne({
        _id: decoded._id,
        'tokens.token': token,
        'tokens.access': 'auth'
    })
}

UserSchema.statics.findByCredentials = function (email, password) {
    var User = this;

    return User.findOne({email}).then((doc) => {
        if (!doc) {
            return Promise.reject();
        }

        return new Promise((resolve, reject) => {
            bcrypt.compare(password, doc.password, (err, res) => {
                if (!res) {
                    reject();
                }

                resolve(doc);
            })
        })
    })
}

UserSchema.methods.removeToken = function (token) {
    var user = this;
    return user.update({
        $pull: {
            tokens: {token}
        }
    })
}

var User = mongoose.model('User', UserSchema)

module.exports = {User};