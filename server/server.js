require('./config/config');
var express = require('express');
var bodyParser = require('body-parser');

var {mongoose} = require('./db/mongoose');
var {Todo} = require('./models/todo');
var {User} = require('./models/user');
var {ObjectID} = require('mongodb');
const _ = require('lodash');
const bcrypt = require('bcryptjs');

//Middleware Authentication
var {auth} = require('./middleware/auth');

var app = express();
const port = process.env.PORT;

app.use(bodyParser.json());

app.post('/todos', auth,  (req, res) => {
    var todo = new Todo ({
        text: req.body.text,
        _creator: req.user._id
    })

    todo.save().then((doc) => {
        res.send(doc);
    }, (e) => {
        res.status(400).send(e);
    })
})

app.get('/todos', auth, (req, res) => {
    Todo.find({
        _creator: req.user._id
    }).then((docs) => {
        res.send({todos: docs});
    }, (e) => {
        res.status(400).send(e);
    })
})

app.get('/todos/:id', auth, (req, res) => {
    if (ObjectID.isValid(req.params.id)) {
        Todo.findOne({
            _id: req.params.id,
            _creator: req.user._id

        }).then((todo) => {
            if (todo) {
                res.send({todo});
            } else {
                res.status(400).send('Todo not found');
            }
        })
    } else {
        res.status(400).send('ID is invalid.');
    }
})

app.delete('/todos/:id', auth, (req, res) => {
    if (ObjectID.isValid(req.params.id)) {
        Todo.findOneAndRemove({
            _id: req.params.id,
            _creator: req.user._id
        }).then((todo) => {
            res.send({message: 'Delete successfully', data: todo})
        }, (e) => {
            res.status(400).send('Todo not found');
        });
    } else {
        res.status(400).send('ID is invalid.');
    }
})

app.patch('/todos/:id', auth, (req, res) => {
    console.log('hihi');
    var id = req.params.id;
    var body = _.pick(req.body, ['text', 'completed']);
    if (ObjectID.isValid(id)) {
        if (_.isBoolean(body.completed) && body.completed) {
            body.completed_at = new Date().getTime();
        } else {
            body.completed = false;
            body.completed_at = null;
        }

        Todo.findOneAndUpdate({
            _id: req.params.id,
            _creator: req.user._id
        }, {$set: body}, {new: true})
        .then((todo) => {
            if (todo) {
                res.send({todo});
            } else {
                res.status(404).send();
            }
        }).catch((e) => {
            res.status(400).send();
        })
    } else {
        res.status(400).send('ID is invalid.');
    }
})

// POST /user
app.post('/users', (req, res) => {
    var body = _.pick(req.body, ['email', 'password']);
    var user = new User(body);

    // User.findByToken()

    user.save()
        .then(() => {
            return user.generateAuthToken();
        }).then((token) => {
            res.header('x-auth', token).send(user);
        })
        .catch((e) => res.status(400).send(e));
})

app.get('/users/me', auth, (req, res) => {
    res.send(req.user);
})

app.delete('/users/logout', auth, (req, res) => {
    req.user.removeToken(req.token).then(() => {
        res.status(200).send('Delete successfully');
    }, () => {
        res.status(400).send('Delete fail');
    }).catch((e) => res.status(400).send(e));
})

// POST /users/login
app.post('/users/login', (req, res) => {
    var body = _.pick(req.body, ['email', 'password']);
    User.findByCredentials(body.email, body.password).then((user) => {

    }).catch((e) => {
        res.status(400).send();
    })
})

app.listen(port, () => {
    console.log('Server is running on port', port);
});

module.exports = {app};