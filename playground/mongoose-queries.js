const {ObjectID} = require('mongodb');
const {mongoose} = require('../server/db/mongoose');
const {Todo} = require('../server/models/todo');
const {User} = require('../server/models/user');

var id = '5aaa3ae9cff8ec0ba033e022';

// Todo.find({
//     _id: id
// }).then((todos) => {
//     console.log('Todos', todos);
// })

// Todo.findOne({
//     _id: id
// }).then((todo) => {
//     console.log('Todos', todo);
// })

// Todo.findById(id).then((todo) => {
//     if (!todo) {
//         return console.log('Id not found');
//     }
//     console.log('Todo by id', todo);
// }).catch((e) => console.log(e));


if (ObjectID.isValid(id)) {
    User.findById(id).then((user) => {
        if (!user) {
            return console.log('User not found');
        }
    
        console.log('User found:', user);
    
    }).catch((e) => {
        console.log('Error:', e);
    })
} else {
    console.log('Id is invalid');
}