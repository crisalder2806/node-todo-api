// const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
  if (err) {
    return console.log('Unable to connect to MongoDB server');
  }
  console.log('Connected to MongoDB server');

  var TodoCollection = db.collection('Users');
  TodoCollection.findOneAndUpdate({
    _id: new ObjectID('5aa6260c5fa78e3d9068a5a7')
  }, {
    $set: {
      name: 'Thanh Giang'
    },
    $inc: {
      age: 1
    }
  }, {
    returnOriginal: false
  }).then((result) => {
    console.log(result)
  })

  db.close();
});
